	<?php $this->load->view('common/head'); ?>
	<link href="<?=base_url()?>assets/css/index.css" rel="stylesheet"/>
	</head>
	<body>
		<div class="container py-5">
			<div class="row">
				<div class="col-12 d-flex justify-content-center align-items-center">
					<img src="<?=base_url()?>assets/img/sigma-logo.png" alt="sigma we code" class="img-fluid" width="23%">
				</div>
				<div class="col-12 pt-5 py-4">
					<h2 class="text-center">Prueba de desarrollo Sigma</h2>
				</div>
				<div class="col-12 px-5 pb-4">
					<p class="text-center px-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				</div>
				<div class="col-12 col-lg-6 d-flex align-items-center justify-content-center">
					<img src="<?=base_url()?>assets/img/sigma-image.png" alt="Formulario" class="img-fluid">
				</div>
				<div class="col-12 col-lg-6">
					<div class="card">
						<div class="card-body p-5">
							<form action="#" onsubmit="return false" >
								<div class="form-group">
									<label for="state" class="font-weight-bolder">Departamento*</label>
									<select name="state" id="state" class="form-control" required msj="Debe seleccionar el departamento">
										<option value="" style="display:none">Seleccionar</option>
										<?php if (count($departamentos)>0) {
											foreach ($departamentos as $d) { ?>
												<option value="<?=$d?>"><?=$d?></option>
										<?php }} ?>
									</select>
								</div>
								<div class="form-group">
									<label for="city" class="font-weight-bolder">Ciudad*</label>
									<select name="city" id="city" class="form-control" required msj="Debe seleccionar la ciudad">
										<option value="" style="display:none">Seleccionar</option>
									</select>
								</div>
								<div class="form-group">
									<label for="name" class="font-weight-bolder">Nombre*</label>
									<input type="text" class="form-control" name="name" required msj="Debe ingresar un nombre" maxlength="50" placeholder="Pepito de Jesús">
								</div>
								<div class="form-group">
									<label for="email" class="font-weight-bolder">Correo*</label>
									<input type="text" class="form-control" name="email" required msj="Debe ingresar un email" maxlength="30" placeholder="Perpitodejesus@gmail.com">
								</div>
								<div class="form-group text-center mb-0 mt-4">
									<button class="btn btn-danger rounded-pill px-5 py-3 font-weight-bolder" id="enviar">ENVIAR</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script type="text/javascript">
		var colombia = <?=json_encode($colombia);?>;

		$('#state').on('change', function() {
			if ($(this).val()!='') {
				let ciudades = colombia[$(this).val()],
					content = '<option value="" style="display:none">Seleccionar</option>'
				for (let i = 0; i < ciudades.length; i++) {
					content += '<option value="'+ciudades[i]+'">'+ciudades[i]+'</option>'
				}
				$('#city').empty().append(content)
			}
		})

		$('#enviar').on('click', function () {
			var data = {},
				bandera = true;
			
			$('form input, form select').each( function () {
				if ($(this).attr('required')!==undefined &&  $(this).val()=='') {
					bandera=false;
					let msj = $(this).attr('msj')
					return Swal.fire( { title: 'Error!', text: msj, icon: 'warning', confirmButtonText: 'Ok' } )
				}else if ($(this).attr('name')=='email' && !validarCorreo($(this).val())) {
					bandera=false;
					return Swal.fire( { title: 'Error!', text: 'Debe ingresar un correo electrónico válido', icon: 'warning', confirmButtonText: 'Ok' } )
				}else{
					data[$(this).attr('name')] = $(this).val()
				}
			})
			
			if (bandera) {
				ajax('welcome/register', data, function(data){
					if (data.res=='ok') {
						Swal.fire( { icon: 'success', title: data.msj, showConfirmButton: false, timer: 3000 } )
						setTimeout(function() { location.reload(); }, 3000);
					}else{
						return Swal.fire( { title: 'Error!', text: data.msj, icon: 'error', confirmButtonText: 'Ok' } )
					}
				})
			}
		})

		function ajax(url,data,done,timeout){
			$.ajax({
				crossDomain: true,
				url: base_url+url,
				context : document.body,
				dataType: "json",
				type: "POST",
				data: data,
				timeout: timeout,
			}).done(done).fail(function(jqXHR,status,msg){
				$("body").removeClass("loading");
				alert("Error conexion: "+status+" - "+msg);
			});
		}

		$(document).ajaxStart(function(){
			$("body").addClass("loading");
		});

		$(document).ajaxStop(function(){
			$("body").removeClass("loading");
		});

		function validarCorreo(correo){
			var regcorreo = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(!regcorreo.test(correo))return false;
			return true;
		}

	</script>
</html>