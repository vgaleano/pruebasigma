<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res = curl_exec($ch);
		curl_close($ch);
		$data['colombia'] = objectToArray(json_decode($res));
		$data['departamentos'] = array_keys($data['colombia']) ;

		$this->load->view('index', $data);
	}

	//servicios
	public function register()
	{
		$data = $this->input->post();
		$this->load->model('Contacts_Model');
		$response = $this->Contacts_Model->store($data);
		if ($response) {
			print json_encode(['res'=>'ok', 'msj'=>'Tu información ha sido recibida satisfactoriamente']);
		}else{
			print json_encode(['res'=>'bad', 'msj'=>'Problemas para registrar la información']);
		}
	}
}
function objectToArray ( $object ) {
	if(!is_object($object) && !is_array($object)) {
	  return $object;
	}  
	return array_map( 'objectToArray', (array) $object );
}
