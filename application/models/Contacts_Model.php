<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     
class Contacts_Model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function store($data)
	{
		$response = $this->db->insert('contacts', $data);
		return $this->db->insert_id();
    } 
}